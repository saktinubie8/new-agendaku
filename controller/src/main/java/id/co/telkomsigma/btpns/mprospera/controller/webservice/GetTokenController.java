package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.request.GetTokenRequest;
import id.co.telkomsigma.btpns.mprospera.response.GetTokenResponse;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static id.co.telkomsigma.btpns.mprospera.util.JWTUtils.createJwt;

@Controller
public class GetTokenController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_TOKEN_PATH, method = {RequestMethod.POST})
    public @ResponseBody
    GetTokenResponse getToken(@RequestBody GetTokenRequest request) {
        try {
            log.info("INCOMING GET TOKEN REQUEST : " + jsonUtils.toJson(request));
        } catch (Exception e2) {
        }
        GetTokenResponse response = new GetTokenResponse();
        response.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        String jwtToken = createJwt(request.getUsername());
        log.info("Session key : " + jwtToken);
        if (!"FAILED".equals(jwtToken)) {
            response.setSessionKey(jwtToken);
        } else {
            response.setResponseCode(WebGuiConstant.RC_GLOBAL_EXCEPTION);
            response.setResponseMessage("CREATE JWT TOKEN FAILED");
            return response;
        }
        response.setResponseCode(WebGuiConstant.RC_SUCCESS);
        String label = getMessage("webservice.rc.label." + response.getResponseCode());
        response.setResponseMessage(label);
        log.debug("Send Response to Handset");
        try {
            log.info("RESPONSE MESSAGE : " + new JsonUtils().toJson(response));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return response;
    }

}