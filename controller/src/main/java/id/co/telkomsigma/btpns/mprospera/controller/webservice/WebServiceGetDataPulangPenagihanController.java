package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangCollectionPojo;
import id.co.telkomsigma.btpns.mprospera.request.AfterPRSPenagihanRequest;
import id.co.telkomsigma.btpns.mprospera.response.AfterPRSResponse;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import id.co.telkomsigma.btpns.mprospera.service.PRSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@Controller("webServiceAfterCollectionrequest")
public class WebServiceGetDataPulangPenagihanController extends GenericController {

    @Autowired
    PRSService prsService;

    @RequestMapping(value = WebGuiConstant.GET_AFTER_COLLECTION_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AfterPRSResponse getRequestDanaCollection(@RequestBody final AfterPRSPenagihanRequest request,
                                              @RequestHeader(value = "requestedDate") final String requestedDate,
                                              @PathVariable("coCode") String coCode) throws Exception {
        JsonUtils jsonUtils = new JsonUtils();
        final AfterPRSResponse responseCode = new AfterPRSResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        log.info("getRequestDataPulangPenagihan INCOMING MESSAGE : " + jsonUtils.toJson(request));
		/*log.info("VALIDATING REQUEST...");
		Boolean wsValidation = verifyJwt(request.getSessionKey(), request.getUsername());
		if (!wsValidation) {
			log.info("Validation failed, invalid session key");
			responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SESSION_KEY);
			String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
			responseCode.setResponseMessage(label);
			return responseCode;

		}
		log.info("Validation success, get request Dana Penagihan");*/
        List<PulangCollectionPojo> afterCollection = prsService.getPulangCollection(coCode, requestedDate);
        if (!afterCollection.isEmpty()) {
            responseCode.setMmsCode(afterCollection.get(0).getWismaCode());
            responseCode.setType("Penagihan");
            responseCode.setCoCode(coCode);
            for (PulangCollectionPojo afterColl : afterCollection) {
                AfterPRSResponse.TypeDetailsPojo pojo = responseCode.new TypeDetailsPojo();
                pojo.setName(afterColl.getSentraName());
                pojo.setAppOrCenterId(afterColl.getSentraId().toString());
                pojo.setAmount(afterColl.getAmount().toString());
                System.out.println(pojo);
                responseCode.getTypeDetails().add(pojo);
            }
        }
        responseCode.setResponseMessage("SUKSES");
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        log.info("Finishing get Data Pulang Penagihan : " + jsonUtils.toJson(responseCode));
        return responseCode;
    }

}