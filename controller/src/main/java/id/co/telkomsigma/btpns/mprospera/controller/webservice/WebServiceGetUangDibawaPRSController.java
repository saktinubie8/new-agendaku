package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.request.BringMoneyPRSRequest;
import id.co.telkomsigma.btpns.mprospera.response.PRSBringMoneyResponse;
import id.co.telkomsigma.btpns.mprospera.service.PRSService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

@Controller("webServiceGetUangDibawaPRSController")
public class WebServiceGetUangDibawaPRSController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    PRSService prsService;

    @RequestMapping(value = WebGuiConstant.GET_BRING_MONEY_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSBringMoneyResponse getUangDibawaPRS(@RequestBody final BringMoneyPRSRequest request) throws Exception {
        final PRSBringMoneyResponse responseCode = new PRSBringMoneyResponse();
        //  Boolean wsValidation = verifyJwt(request.getSessionKey(),request.getUsername());
      /*  if(!wsValidation){
            log.info("Validation failed, invalid session key");
            responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SESSION_KEY);
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            return responseCode;
        }else {*/
        BigDecimal ammount = prsService.getBringMoney(request.getUsername(), request.getPrsDate(), request.getWismaCode());
        responseCode.setAmount(ammount == null ? "0" : ammount.toString());
        //}
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setResponseMessage("SUKSES");
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        log.info("getUangDibawaPRS INCOMING MESSAGE : " + jsonUtils.toJson(request));
        return responseCode;
    }

}