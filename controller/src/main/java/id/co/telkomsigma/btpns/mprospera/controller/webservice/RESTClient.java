package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.request.ProsperaLoginRequest;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Date;

@Component("restClient")
public class RESTClient {

    protected final Log LOGGER = LogFactory.getLog(getClass());
    private String HOST;
    private String PORT;
    private String MAIN_URI;

    @Autowired
    private ParameterService parameterService;

    @PostConstruct
    protected void init() {
        HOST = parameterService.loadParamByParamName("esb.host", "https://10.7.17.163");
        PORT = parameterService.loadParamByParamName("esb.port", "8443");
        MAIN_URI = parameterService.loadParamByParamName("esb.uri", "/btpns/mprospera");
    }

    public Boolean echoSuccess() {
        String url = HOST + ":" + PORT + MAIN_URI + "/echo";
        RestTemplate restTemplate = getRestTemplate();
        try {
            LOGGER.debug("SEND ECHO...");
            Date start = new Date();
            ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
            LOGGER.debug("ECHO SUCCESS in " + (new Date().getTime() - start.getTime()) + " ms");
            return response.getStatusCode().is2xxSuccessful();
        } catch (HttpClientErrorException e) {
            HttpStatus httpStatus = e.getStatusCode();
            LOGGER.error("ECHO FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
            return e.getStatusCode().is2xxSuccessful();
        } catch (Exception e) {
            LOGGER.error("ECHO FAILED..., " + e.getMessage());
            return false;
        }
    }

    public ProsperaLoginResponse login(ProsperaLoginRequest request) throws NoSuchAlgorithmException, KeyManagementException {
        String url = HOST + ":" + PORT + MAIN_URI + "/login";
        LOGGER.debug("POST URL : " + url);
        ProsperaLoginResponse response = new ProsperaLoginResponse();
        try {
            LOGGER.debug("Request payload : " + request.toString());
            response = getRestTemplate().postForObject(url, request, ProsperaLoginResponse.class);
            LOGGER.info("Got response..., " + response.toString());
            return response;
        } catch (HttpClientErrorException e) {
            HttpStatus httpStatus = e.getStatusCode();
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("login ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
            return response;
        } catch (Exception e) {
            response.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            response.setResponseMessage("login ESB Error: " + e.getMessage());
            return response;
        }
    }

    protected RestTemplate getRestTemplate() {
        // Create a trust manager that does not validate certificate chains
        Date start = new Date();
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws CertificateException {
            }
        }};
        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e1) {
            LOGGER.error("SSL INSTANCE FETCHING FAILED..., " + e1.getMessage());
        }
        try {
            if (sc != null) {
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            }
        } catch (KeyManagementException e) {
            LOGGER.error("SSL CONTEXT INITIALIZING FAILED..., " + e.getMessage());
        }
        // Create all-trusting host name verifier
        final HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        RestTemplate result = new RestTemplate(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                Date prepareConnectionTime = new Date();
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier(allHostsValid);
                }
                super.prepareConnection(connection, httpMethod);
                LOGGER.trace("prepareConnectionTime duration: " + (new Date().getTime() - prepareConnectionTime.getTime()) + " in ms");
            }
        });
        LOGGER.trace("restClient duration: " + (new Date().getTime() - start.getTime()) + " in ms");
        return result;
    }

}