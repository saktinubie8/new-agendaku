package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.request.GetDanaRequest;
import id.co.telkomsigma.btpns.mprospera.response.PRSBringMoneyResponse;
import id.co.telkomsigma.btpns.mprospera.service.PRSService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

@Controller("webServiceGetDanaPRSController")
public class WebServiceGetDanaPRSController extends GenericController {

    @Autowired
    PRSService prsService;

    @RequestMapping(value = WebGuiConstant.GET_MMS_MONEY_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    PRSBringMoneyResponse getRequestDanaPRS(@RequestBody final GetDanaRequest request
    ) throws Exception {
        JsonUtils jsonUtils = new JsonUtils();
        final PRSBringMoneyResponse responseCode = new PRSBringMoneyResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        log.info("getRequestDanaPRS INCOMING MESSAGE : " + jsonUtils.toJson(request));
        /*log.info("VALIDATING REQUEST...");
        Boolean wsValidation = verifyJwt(request.getSessionKey(),request.getUsername());
        if(!wsValidation){
            log.info("Validation failed, invalid session key");
            responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SESSION_KEY);
            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
            responseCode.setResponseMessage(label);
            return responseCode;
        }
        log.info("Validation success, get request Dana PRS");*/
        BigDecimal totalDana = prsService.getPlafonplusWithDarwalNext(request.getWismaCode(), request.getLastPrsDate());
        responseCode.setAmount(totalDana == null ? "0" : totalDana.toString());
        responseCode.setResponseMessage("SUKSES");
        log.info("Finishing Get Dana PRS : " + jsonUtils.toJson(responseCode));
//        BigDecimal totalAmount = prsService.getAmmount(request.getWismaCode(), request.getLastPrsDate());
//        responseCode.setAmount(totalAmount == null ? "0" : totalAmount.toString());
//        responseCode.setResponseMessage("SUKSES");
//        log.info("Finishing get Dana PRS");
        return responseCode;
    }

}