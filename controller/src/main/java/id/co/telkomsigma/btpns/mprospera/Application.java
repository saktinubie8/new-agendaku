package id.co.telkomsigma.btpns.mprospera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import static id.co.telkomsigma.btpns.mprospera.util.JWTUtils.createJwt;
import static id.co.telkomsigma.btpns.mprospera.util.JWTUtils.verifyJwt;

@SpringBootApplication(exclude = {JmxAutoConfiguration.class})
@EnableCaching
@ComponentScan
@Configuration
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args){
        SpringApplication microService = new SpringApplication(Application.class);
        microService.run(args);
      /*  String username = "w0211f";
        String token = createJwt(username);
        System.out.println("JWT : "+token);
       if(verifyJwt(token,username)){
           System.out.println("OKE");
       }else{
           System.out.println("FAILED");
       }*/
    }

}