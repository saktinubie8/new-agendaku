package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangPRSpojo;
import id.co.telkomsigma.btpns.mprospera.request.AfterPRSPenagihanRequest;
import id.co.telkomsigma.btpns.mprospera.response.AfterPRSResponse;
import id.co.telkomsigma.btpns.mprospera.service.PRSService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller("webServiceAfterPRSrequest")
public class WebServiceGetDataPulangPRSController extends GenericController {

    @Autowired
    PRSService prsService;

    @RequestMapping(value = WebGuiConstant.GET_AFTER_PRS_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AfterPRSResponse getRequestDanaPRS(@RequestBody final AfterPRSPenagihanRequest request,
                                       @RequestHeader(value = "requestedDate") final String requestedDate,
                                       @PathVariable("coCode") String coCode) throws Exception {
        JsonUtils jsonUtils = new JsonUtils();
        final AfterPRSResponse responseCode = new AfterPRSResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        log.info("getRequestDataPulangPRS INCOMING MESSAGE : " + jsonUtils.toJson(request));
		/*log.info("VALIDATING REQUEST...");
		Boolean wsValidation = verifyJwt(request.getSessionKey(), request.getUsername());
		if (!wsValidation) {
			log.info("Validation failed, invalid session key");
			responseCode.setResponseCode(WebGuiConstant.RC_INVALID_SESSION_KEY);
			String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
			responseCode.setResponseMessage(label);
			return responseCode;

		}
		log.info("Validation success, get request Dana PRS");*/
        List<PulangPRSpojo> afterPRS = prsService.getPulangPRS(coCode, requestedDate);
        if (!afterPRS.isEmpty()) {
            responseCode.setMmsCode(afterPRS.get(0).getWismaCode());
            responseCode.setType("PRS");
            responseCode.setCoCode(coCode);
            for (PulangPRSpojo afterPrs : afterPRS) {
                AfterPRSResponse.TypeDetailsPojo pojo = responseCode.new TypeDetailsPojo();
                pojo.setName(afterPrs.getSentraName());
                pojo.setAppOrCenterId(afterPrs.getSentraId().toString());
                pojo.setAmount(afterPrs.getAmount().toString());
                System.out.println(pojo);
                responseCode.getTypeDetails().add(pojo);
            }
        }
        responseCode.setResponseMessage("SUKSES");
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        log.info("Finishing get Data Pulang PRS : " + jsonUtils.toJson(responseCode));
        return responseCode;
    }

}