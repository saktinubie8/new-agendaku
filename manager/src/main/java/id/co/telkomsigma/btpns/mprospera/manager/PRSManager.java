package id.co.telkomsigma.btpns.mprospera.manager;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.pojo.PulangCollectionPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangPRSpojo;

public interface PRSManager {


    BigDecimal getDisbursementAmt(String wismaCode, Date prsDate);

    BigDecimal getWithdrawalAmtPlan(String wismaCode, Date prsDate);

    BigDecimal getBringMoney(String wismaCode, Date prsDate, String coCode);

    BigDecimal getplafon(Date disbrusmenDate, String wismaCode, String status);

    BigDecimal getwithdrawalAmtNext(String wismaCode, Date prsDate);

    List<PulangPRSpojo> getPulangPRS(String coCode, Date requestedDate);

    List<PulangCollectionPojo> getPulangCollection(String coCode, Date requestedDate);

}