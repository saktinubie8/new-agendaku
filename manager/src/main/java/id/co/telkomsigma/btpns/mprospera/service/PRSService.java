package id.co.telkomsigma.btpns.mprospera.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.PRSManager;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangCollectionPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.PulangPRSpojo;

@Service("prsService")
public class PRSService extends GenericService{
	
	@Autowired
	PRSManager prsManager;
	
	public BigDecimal getAmmount(String wismaCode,String date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		BigDecimal disbursementAmt = prsManager.getDisbursementAmt(wismaCode, formatter.parse(date));
		if(disbursementAmt == null){
			disbursementAmt = BigDecimal.ZERO;
		}
		BigDecimal withdrawalAmtPlan = prsManager.getWithdrawalAmtPlan(wismaCode, formatter.parse(date));
		if(withdrawalAmtPlan == null){
			withdrawalAmtPlan = BigDecimal.ZERO;
		}
		disbursementAmt=disbursementAmt.add(withdrawalAmtPlan);
		return disbursementAmt;
	}
	
	public BigDecimal getPlafonplusWithDarwalNext(String wismaCode, String date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		BigDecimal withDrawalAmtNext = prsManager.getwithdrawalAmtNext(wismaCode,  formatter.parse(date));
		if(withDrawalAmtNext == null){
			withDrawalAmtNext = BigDecimal.ZERO;
		}
		BigDecimal plafon = prsManager.getplafon(formatter.parse(date), wismaCode, WebGuiConstant.RC_APPROVED);
		if(plafon == null){
			plafon = BigDecimal.ZERO;
		}
		withDrawalAmtNext = withDrawalAmtNext.add(plafon);
		return withDrawalAmtNext;
	}
	
	public BigDecimal getBringMoney(String coCode,String date,String wismaCode) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		BigDecimal bringMoney = prsManager.getBringMoney(wismaCode, formatter.parse(date),coCode);
		return bringMoney;
	}
	
	public List<PulangPRSpojo> getPulangPRS(String coCode,String requestedDate) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		List<PulangPRSpojo> listPRS = prsManager.getPulangPRS(coCode, formatter.parse(requestedDate));
		
		return listPRS;
	}
	
	public List<PulangCollectionPojo> getPulangCollection(String coCode,String requestedDate) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		List<PulangCollectionPojo> listCollection = prsManager.getPulangCollection(coCode, formatter.parse(requestedDate));
		
		return listCollection;
	}

}
