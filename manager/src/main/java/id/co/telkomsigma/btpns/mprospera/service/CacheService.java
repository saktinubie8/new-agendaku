package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    ParamManager paramManager;

    public void clearCache() {
        paramManager.clearCache();
    }

}