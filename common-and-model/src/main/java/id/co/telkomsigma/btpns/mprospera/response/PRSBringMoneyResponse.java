package id.co.telkomsigma.btpns.mprospera.response;

public class PRSBringMoneyResponse extends BaseResponse {

    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}