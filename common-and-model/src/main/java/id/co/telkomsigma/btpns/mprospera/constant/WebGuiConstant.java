package id.co.telkomsigma.btpns.mprospera.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {

    /**
     * URL GLOBAL
     */
    public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";
    public static final String TERMINAL_GET_TOKEN_PATH = "/webservice/getToken**";
    public static final String TERMINAL_TEST_GET_TOKEN_PATH = "/webservice/getToken**";

    /**
     * RESPONSE CODE
     */
    public static final String RC_SUCCESS = "00";
    public static final String RC_FAILED_LOGIN = "01";
    public static final String RC_RESET_PASSWORD_FAILED = "02";
    public static final String RC_USER_DISABLE = "03";
    public static final String RC_TERMINAL_NOT_FOUND = "04";
    public static final String RC_GLOBAL_EXCEPTION = "05";
    public static final String RC_ERROR_SCORING = "06";
    public static final String RC_ROLE_CANNOT_ACCESS_MOBILE = "07";
    public static final String RC_INVALID_SDA_CATEGORY = "08";
    public static final String RC_KFO_CUT_OFF = "09";
    public static final String RC_CREATE_SW_FAILED = "10";
    public static final String RC_SW_ALREADY_EXIST = "11";
    public static final String RC_DELETE_SW_FAILED = "12";
    public static final String RC_CIF_NOT_FOUND = "13";
    public static final String RC_UPDATE_SW_FAILED = "14";
    public static final String RC_INVALID_APK = "15";
    public static final String RC_UNKNOWN_MM_ID = "16";
    public static final String RC_UNKNOWN_PM_ID = "17";
    public static final String RC_UNKNOWN_LOAN_ID = "18";
    public static final String RC_UNKNOWN_SURVEY_ID = "19";
    public static final String RC_CREATE_SENTRA_FAILED = "20";
    public static final String RC_SENTRA_ALREADY_EXIST = "21";
    public static final String RC_UPDATE_SENTRA_FAILED = "22";
    public static final String RC_SENTRA_ID_NOT_FOUND = "23";
    public static final String RC_ASSIGN_PS_ERROR = "24";
    public static final String RC_CREATE_GROUP_FAILED = "25";
    public static final String RC_GROUP_ALREADY_EXIST = "26";
    public static final String RC_DELETE_GROUP_FAILED = "27";
    public static final String RC_UPDATE_GROUP_FAILED = "28";
    public static final String RC_GROUP_ID_NOT_FOUND = "29";
    public static final String RC_CREATE_CUSTOMER_FAILED = "30";
    public static final String RC_CUSTOMER_ALREADY_EXIST = "31";
    public static final String RC_TRANSACTION_INQUIRY_ERROR = "32";
    public static final String RC_SENTRAGROUP_NOT_APPROVED = "33";
    public static final String RC_SENTRAGROUP_NULL = "34";
    public static final String RC_CREATE_LOAN_FAILED = "35";
    public static final String RC_LOAN_ALREADY_EXIST = "36";
    public static final String RC_DELETE_LOAN_FAILED = "37";
    public static final String RC_APPROVAL_DEVIATION_FAILED = "38";
    public static final String RC_CLAIM_INSURANCE_FAILED = "39";
    public static final String RC_SUBMIT_PRS_FAILED = "40";
    public static final String RC_INQUIRY_PRS_ERROR = "41";
    public static final String RC_ID_PHOTO_NULL = "42";
    public static final String RC_SURVEY_PHOTO_NULL = "43";
    public static final String RC_PRS_REJECTED = "44";
    public static final String RC_UNKNOWN_SAVING = "45";
    public static final String RC_NO_LOAN_HIST = "46";
    public static final String RC_NO_SAVING_HIST = "47";
    public static final String RC_INVALID_KECAMATAN = "48";
    public static final String RC_INVALID_KELURAHAN = "49";
    public static final String RC_SUBMIT_COLLECTION_FAILED = "50";
    public static final String RC_AP3R_ALREADY_EXIST = "51";
    public static final String RC_SW_PRODUCT_NULL = "52";
    public static final String RC_SW_APPROVED_FAILED = "53";
    public static final String RC_PHOTO_ALREADY_EXIST = "54";
    public static final String RC_USER_CANNOT_APPROVE = "55";
    public static final String RC_UNKNOWN_INSURANCE_CLAIM_ID = "56";
    public static final String RC_FILE_DAYA_NULL = "57";
    public static final String RC_EARLY_TERMINATION_PLAN_NULL = "58";
    public static final String RC_UNREGISTERED_USER = "59";
    public static final String RC_CUSTOMER_WAIT_APPROVED = "60";
    public static final String RC_LOCATION_ID_NOT_FOUND = "61";
    public static final String RC_APPID_NULL = "62";
    public static final String RC_GROUP_LEADER_NULL = "63";
    public static final String RC_INVALID_RRN = "64";
    public static final String RC_AP3R_NULL = "65";
    public static final String RC_DEVIATION_NOT_APPROVED = "66";
    public static final String RC_DEVIATION_NULL = "67";
    public static final String RC_SENTRA_PHOTO_NULL = "68";
    public static final String RC_SW_NOT_APPROVED = "69";
    public static final String RC_DEVIATION_CANNOT_DELETE = "70";
    public static final String RC_EOD_PROSPERA_IS_RUNNING = "91";
    public static final String RC_GENERATE_AP3R_FAILED = "A9";
    public static final String RC_ESB_UNAVAILABLE = "SU";
    public static final String RC_INVALID_SESSION_KEY = "B1";
    public static final String RC_UNKNOWN_PARENT_AREA = "B2";
    public static final String RC_UNKNOWN_SDA_ID = "B3";
    public static final String RC_UNKNOWN_SENTRA_ID = "B4";
    public static final String RC_UNKNOWN_PDK_ID = "B5";
    public static final String RC_UNKNOWN_SW_ID = "B6";
    public static final String RC_UNKNOWN_CUSTOMER_ID = "B7";
    public static final String RC_USERNAME_NOT_EXISTS = "B8";
    public static final String RC_IMEI_NOT_EXISTS = "B9";
    public static final String RC_SESSION_KEY_NOT_EXISTS = "C1";
    public static final String RC_IMEI_NOT_VALID_FORMAT = "C2";
    public static final String RC_USER_INVALID_CITY = "C3";
    public static final String RC_SENTRA_ALREADY_APPROVED = "C4";
    public static final String RC_UNKNOWN_STATUS = "C5";
    public static final String RC_ALREADY_CLOSED = "C6";
    public static final String RC_ALREADY_REJECTED = "C7";
    public static final String RC_ALREADY_CANCEL = "C8";
    public static final String RC_SENTRA_NOT_APPROVED = "C9";
    public static final String RC_UNKNOWN_APPID = "D1";
    public static final String RC_USERNAME_USED = "D2";
    public static final String RC_UNKNOWN_CIF_NUMBER = "D3";
    public static final String RC_PRS_NULL = "D4";
    public static final String RC_PRS_PHOTO_NULL = "D5";
    public static final String RC_PRS_ALREADY_SUBMIT = "D6";
    public static final String RC_UNKNOWN_LOAN_PRS = "D7";
    public static final String RC_AP3R_ALREADY_APPROVED = "D8";
    public static final String RC_ROLE_NULL = "D9";
    public static final String RC_SW_ALREADY_APPROVED = "E1";
    public static final String RC_RECORD_ALREADY_APPROVED = "E2";
    public static final String RC_SERVER_TIMEOUT = "ZZ";
    public static final String RC_INVALID_USER_PASSWORD = "1A";
    public static final String RC_USER_LOCKED = "1B";
    public static final String RC_USER_INACTIVE = "1C";
    public static final String RC_GENERAL_ERROR = "XX";
    /**
     * URL WebService Get Request Dana PRS
     */
    public static final String GET_MMS_MONEY_PRS = "/webservice/getRequestDana**";
    public static final String GET_MMS_MONEY_PRS_REQUEST = GET_MMS_MONEY_PRS;

    /**
     * URL WebService Get Data Pulang PRS
     */
    public static final String GET_AFTER_PRS = "/webservice/prs**";
    public static final String GET_AFTER_PRS_REQUEST = GET_AFTER_PRS + "/{coCode:.+}";

    /**
     * URL WebService Get Request Dana Collection
     */
    public static final String GET_AFTER_COLLECTION = "/webservice/penagihan**";
    public static final String GET_AFTER_COLLECTION_REQUEST = GET_AFTER_COLLECTION + "/{coCode:.+}";

    /**
     * URL WebService Get Uang dibawa PRS
     */
    public static final String GET_BRING_MONEY = "/webservice/getPRSMoney**";
    public static final String GET_BRING_MONEY_REQUEST = GET_BRING_MONEY;

    public static final String CLEAR_ALL_CACHE = "/clearCache**";

    // custom parameter
    public static final String PARAMETER_SECRET_KEY = "secret.key";
    public static final String RC_APPROVED = "APPROVED";

}