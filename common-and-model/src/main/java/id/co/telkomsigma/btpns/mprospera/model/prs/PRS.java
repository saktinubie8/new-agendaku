package id.co.telkomsigma.btpns.mprospera.model.prs;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

@Entity
@Table(name = "T_PRS")
public class PRS extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -125248653725693582L;

    private Long prsId;
    private Date createdDate;
    private String createdBy;
    private Date updateDate;
    private String updateBy;
    private Sentra sentraId;
    private Date prsDate;
    private String prsTime;
    private String prsEndTime;
    private String status;
    private String psCompanion;
    private BigDecimal bringMoney;
    private BigDecimal actualMoney;
    private BigDecimal paymentMoney;
    private BigDecimal withdrawalAdhoc;
    private String report;
    private String latitude;
    private String longitude;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getPrsId() {
        return prsId;
    }

    public void setPrsId(Long prsId) {
        this.prsId = prsId;
    }

    @Column(name = "created_date", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Sentra.class)
    @JoinColumn(name = "SENTRA_ID", referencedColumnName = "id")
    public Sentra getSentraId() {
        return sentraId;
    }

    public void setSentraId(Sentra sentraId) {
        this.sentraId = sentraId;
    }

    @Column(name = "PRS_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    public Date getPrsDate() {
        return prsDate;
    }

    public void setPrsDate(Date prsDate) {
        this.prsDate = prsDate;
    }

    @Column(name = "PRS_END_TIME")
    public String getPrsEndTime() {
        return prsEndTime;
    }

    public void setPrsEndTime(String prsEndTime) {
        this.prsEndTime = prsEndTime;
    }

    @Column(name = "PRS_STATUS", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "PS_COMPANION")
    public String getPsCompanion() {
        return psCompanion;
    }

    public void setPsCompanion(String psCompanion) {
        this.psCompanion = psCompanion;
    }

    @Column(name = "update_date")
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Column(name = "update_by")
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    @Column(name = "bring_money")
    public BigDecimal getBringMoney() {
        return bringMoney;
    }

    public void setBringMoney(BigDecimal bringMoney) {
        this.bringMoney = bringMoney;
    }

    @Column(name = "actual_money")
    public BigDecimal getActualMoney() {
        return actualMoney;
    }

    public void setActualMoney(BigDecimal actualMoney) {
        this.actualMoney = actualMoney;
    }

    @Column(name = "payment_money")
    public BigDecimal getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(BigDecimal paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

    @Column(name = "withdrawal_adhoc")
    public BigDecimal getWithdrawalAdhoc() {
        return withdrawalAdhoc;
    }

    public void setWithdrawalAdhoc(BigDecimal withdrawalAdhoc) {
        this.withdrawalAdhoc = withdrawalAdhoc;
    }

    @Column(name = "prs_time")
    public String getPrsTime() {
        return prsTime;
    }

    public void setPrsTime(String prsTime) {
        this.prsTime = prsTime;
    }

    @Column(name = "report")
    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    @Column(name = "latitude")
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Column(name = "longitude")
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}