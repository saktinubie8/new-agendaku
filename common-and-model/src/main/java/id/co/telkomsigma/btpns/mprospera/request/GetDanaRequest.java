package id.co.telkomsigma.btpns.mprospera.request;

public class GetDanaRequest extends BaseRequest {

    private String wismaCode;
    private String lastPrsDate;
    private String username;
    private String sessionKey;

    public String getWismaCode() {
        return wismaCode;
    }

    public void setWismaCode(String wismaCode) {
        this.wismaCode = wismaCode;
    }

    public String getLastPrsDate() {
        return lastPrsDate;
    }

    public void setLastPrsDate(String lastPrsDate) {
        this.lastPrsDate = lastPrsDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

}