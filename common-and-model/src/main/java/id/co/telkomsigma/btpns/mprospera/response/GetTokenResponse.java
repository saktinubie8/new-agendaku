package id.co.telkomsigma.btpns.mprospera.response;

public class GetTokenResponse extends BaseResponse {

    private String sessionKey;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

}