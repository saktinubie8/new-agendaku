package id.co.telkomsigma.btpns.mprospera.request;

public class AfterPRSPenagihanRequest extends BaseRequest {

    private String username;
    private String sessionKey;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

}